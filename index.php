<!DOCTYPE html>
  <html lang="en" class="no-js">

    <head>
  
        <link rel="stylesheet" type="text/css" href="jsgantt.css" />
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" />
       
      <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Cookie|Satisfy|Kelly+Slab|Overlock" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    </head>
    <body>
      
      <div id="embedded-Gantt"></div>

      <script language="javascript" src="jsgantt.js"></script>

          <!-- Font Awesome -->
      <script src="https://use.fontawesome.com/78d1e57168.js"></script>

      <script type="text/javascript">

      function loadGantt()
      {
        var g = new JSGantt.GanttChart(document.getElementById('embedded-Gantt'), 'month');
        if (g.getDivId() != null) {
          g.setUseSort(0);
          g.setUseToolTip(0);
          g.setCaptionType('Complete');  // Set to Show Caption (None,Caption,Resource,Duration,Complete)
          g.setQuarterColWidth(50);
          //g.setRowHeight(100);
          g.setShowPlanStartDate(1);
          // g.setShowTaskInfoStartDate(1);
          // g.setShowTaskInfoEndDate(1);
          g.setShowPlanEndDate(1);
          // g.setDayColWidth(21);
          g.setDateTaskDisplayFormat('day dd month yyyy'); // Shown in tool tip box
          g.setDayMajorDateDisplayFormat('mon yyyy - Week ww') // Set format to display dates in the "Major" header of the "Day" view
          g.setWeekMinorDateDisplayFormat('dd mon') // Set format to display dates in the "Minor" header of the "Week" view
          g.setShowTaskInfoLink(1); // Show link in tool tip (0/1)
          g.setShowComp(0);
          g.setShowRes(0);
          g.setShowDeps(1);
          g.setShowEndWeekDate(0); // Show/Hide the date for the last day of the week in header for daily view (1/0)
          g.setUseSingleCell(10000); // Set the threshold at which we will only use one cell per table row (0 disables).  Helps with rendering performance for large charts.
          g.setFormatArr('Day', 'Week', 'Month', 'Quarter'); // Even with setUseSingleCell using Hour format on such a large chart can cause issues in some browsers
          
          var criticalid = [];
         
                  //alert(full_array[activity_id])



          
          // Parameters                     (pID, pName,                  pStart,       pEnd,        pStyle,         pLink (unused)  pMile, pRes,       pComp, pGroup, pParent, pOpen, pDepend, pCaption, pNotes, pGantt)
           // g.AddTaskItem(new JSGantt.TaskItem(1,   'Define Chart API',     '',           '',          'ggroupblack',  '',                 0, 'Brian',    0,     1,      0,       '',     '',      '',      'Some Notes text', g ));
           // g.AddTaskItem(new JSGantt.TaskItem(11,  'Chart Object',         '2019-08-20','2020-03-06', 'gmilestone',   '',                 1, 'Shlomy',   100,   0,      1,       1,     '',      '',      '',      g));
           g.AddTaskItem(new JSGantt.TaskItem(1,  'Task Objects',         '',           '',          'ggroupblack',  '',                 0, 'Shlomy',   40,    1,      0,       '',     '',      '',      '',      g));
           g.AddTaskItem(new JSGantt.TaskItem(121, 'Constructor Proc',     '2019-08-20','2020-03-06', 'gtaskblue',    '',                 0, 'Brian T.', 60,    0,      1,      1,     '',      '',      '',      g));
          g.AddTaskItem(new JSGantt.TaskItem(122, 'Task Variables',       '2019-08-20','2020-03-06', 'gtaskred',     '',                 0, 'Brian',    60,    0,      1,      1,     121,     '',      '',      g));
           g.AddTaskItem(new JSGantt.TaskItem(123, 'Task by Minute/Hour',  '2019-08-20','2020-03-06 12:00', 'gtaskyellow', '',            0, 'Ilan',     60,    0,      1,      1,     '',      '',      '',      g));
           g.AddTaskItem(new JSGantt.TaskItem(124, 'Task Functions',       '2019-08-20','2020-03-06', 'gtaskred',     '',                 0, 'Anyone',   60,    0,      1,      1,     '123SS', 'This is a caption', null, g));
          
           

          g.Draw();
        } else {
          alert("Error, unable to create Gantt Chart");
        }
      }

        $( document ).ready(function() {
          
          loadGantt();
        });    
    
      </script>
    </body>
</html>